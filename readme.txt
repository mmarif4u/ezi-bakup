Bash script to create backup of your web server directory and mysql database.
Feel free to distribute, modify and use.
Author: Arif
Version: 0.2
License: GPLv3 [http://www.gnu.org/licenses/gpl-3.0.html]
Release: 30-08-2013
NOTE: Please change the vars values in params.ini

How to use:
- Read article https://www.therandombits.com/1214/ezi-bakup/ for more details.

Cron sample setup:
- Read article https://www.therandombits.com/1214/ezi-bakup/ for more details.